# Import the required modules
import os
import webbrowser
import copy
import random
import requests
import numpy as np
import pandas as pd
import geopandas
from datetime import datetime, timedelta
from bokeh.plotting import figure, output_file, save
from bokeh.models import Panel, Tabs, ColumnDataSource, HoverTool, Legend, Div, CheckboxGroup, LogColorMapper, ColorBar, GeoJSONDataSource, NumeralTickFormatter, DatetimeTickFormatter, CustomJS, LogTicker
from bokeh.layouts import row, column
from bokeh.palettes import brewer
from bs4 import BeautifulSoup
from bokeh.tile_providers import WIKIMEDIA, get_provider
from sklearn.linear_model import LinearRegression

# Initialise a random seed so we get the same colours every time
random.seed(0)

# The URL containing the data
json_url = 'https://pomber.github.io/covid19/timeseries.json'

# Function to return a random colour
def random_colour():
    return tuple([random.randrange(255), random.randrange(255), random.randrange(255)])

# The country we're looking at
countries = ['France',
             'United Kingdom',
             'China',
             'US',
             'Brazil',
             'Australia',
             'India',
             'Sweden',
             'Germany',
             'Russia',
             'Philippines',
             'Nigeria',
             'Saudi Arabia',
             'South Africa',
             'Mexico',
             'Spain']

# Some common chart defaults
plot_width  = 1200
plot_height = 650
tools       = 'pan, wheel_zoom, reset'

# Constant used to decide how to align data
start_after = 3

# Constant used to set the size of the rolling window
rolling_window = 7

# Name of the output file
temp_filename   = 'temp/temp.html'
output_filename = 'temp/index.html'
locations_data  = 'data/locations.csv'

# Create a glyph dict, indexable by country name,
# to contain arrays of glyphs to be turned on and off by the check boxes
glyph_dict = {}

# Create a colour dictionary to contain the colours of the countries
colour_dict = {}

# Function to convert Lat, Long to Web Mercator
def geographic_to_web_mercator(lat, long):
    num = long * 0.017453292519943295
    x = 6378137.0 * num
    a = lat * 0.017453292519943295
    x_mercator = x
    y_mercator = 3189068.5 * np.log((1.0 + np.sin(a)) / (1.0 - np.sin(a)))
    return x_mercator, y_mercator

# Function to get the data from the internet
def getData():
    # Get the data from the internet
    raw_data = requests.get(json_url)

    # Turn the data into a json object
    json_data = raw_data.json()

    # Return the data
    return json_data

# Parse a country's data into a Pandas DataFrame
def ParseData(json_data):
    # Create a dictionary, indexable by country name, to contain the data frames
    full_df_dict = {}

    # Loop through each country in the json data, create a data frame and add it to the dictionary
    for country in json_data:
        full_df_dict[country] = pd.DataFrame(json_data[country])
        # Convert the date strings to proper datetime objects
        full_df_dict[country]['date'] = pd.to_datetime(full_df_dict[country]['date'])

    # Whole world stats are not available, so create an empty data frame to contain them
    world_df = pd.DataFrame(data = np.zeros(full_df_dict['US'].shape), columns=full_df_dict['US'].columns)

    # Set the dates from the US data frame as US is the least typing...
    world_df['date'] = pd.to_datetime(full_df_dict['US']['date'])

    # Loop through the countries once more to sum the columns to generate world stats
    for country in full_df_dict:
        world_df['confirmed'] += full_df_dict[country]['confirmed']
        world_df['deaths']    += full_df_dict[country]['deaths']
        world_df['recovered'] += full_df_dict[country]['recovered']

    # Add the world entry to the dictionary
    full_df_dict['World'] = world_df.reset_index()

    # Add the dailies
    InsertDailyColumns(full_df_dict)

    # Add the running averages
    CreateRunningAverages(full_df_dict)

    # Add an entry for World to the list of countries (world is always black, not random)
    colour_dict['World'] = 'black'
    glyph_dict['World'] = []

    print('Saving Data...')

    for country in full_df_dict:
        full_df_dict[country].to_pickle('pickles/' + country + '.pickle')

    print('Data Saved')

    return full_df_dict

# Parse the country specific data
def ParseCountryData(full_df_dict):
    # Read in the country data
    static_country_data = pd.read_csv(locations_data)

    # Read in the countries shape file
    geo_json_df = geopandas.read_file('data/ne_110m_admin_0_countries/ne_110m_admin_0_countries.geojson')

    # Merge the geographic data with the country data
    country_data = geopandas.GeoDataFrame(static_country_data.merge(geo_json_df, right_on='ADM0_A3', left_on='ISO Codes'))

    # Create a list to contain the confirmed cases
    confirmed_list = list()

    # Loop through the countries to add the latest number of confirmed cases
    for country in country_data['Country']:
        country_df = full_df_dict[country]
        confirmed_list.append(country_df['confirmed'][len(country_df)-1])

        # Add the country name to the glyph dict
        glyph_dict[country] = []

        # Create a colour for this country
        colour_dict[country] = random_colour()

    # Add the list of confirmed cases to the country specific data
    country_data['Confirmed']  = confirmed_list
    country_data['ConfPerCap'] = ((country_data['Confirmed'] / country_data['Population']) * 100000000).round()

    # Generate sizes for circle plots for each country
    country_data['ConfirmedSize'] = (np.log2(country_data['Confirmed']) / np.log2(1.5)).round()
    country_data['ConfPerCapSize'] = (np.log2(country_data['ConfPerCap']) / np.log2(1.5)).round()

    # Convert latitude and longitude to Web Mercator
    country_data['WM_x'], country_data['WM_y'] = geographic_to_web_mercator(country_data['Latitude'],
                                                                            country_data['Longitude'])

    # Set the coordinate reference system EPSG:4326 (WGS84 Lat/Long)
    country_data.crs = 'epsg:4326'

    # Tell GeoPandas that the column named geometry contains the points describing the countries
    country_data.set_geometry('geometry')

    return country_data

# Function to add columns for daily data
def InsertDailyColumns(full_df_dict):
    for country in full_df_dict:
        # Use diff() to calculate the difference between this and last down the column
        daily_confirmed = full_df_dict[country]['confirmed'].diff()
        daily_deaths    = full_df_dict[country]['deaths'].diff()
        daily_recovered = full_df_dict[country]['recovered'].diff()

        # diff() sets the first entry to NaN, so set it to the first element value
        daily_confirmed[0] = full_df_dict[country]['confirmed'][0]
        daily_deaths[0]    = full_df_dict[country]['deaths'][0]
        daily_recovered[0] = full_df_dict[country]['recovered'][0]

        # Insert the new columns
        full_df_dict[country]['DailyConfirmed'] = daily_confirmed
        full_df_dict[country]['DailyDeaths'] = daily_deaths
        full_df_dict[country]['DailyRecovered'] = daily_recovered

        # Add a column for the active cases
        full_df_dict[country]['ActiveCases'] = full_df_dict[country]['confirmed'] -\
                                               (full_df_dict[country]['deaths'] + full_df_dict[country]['recovered'])

# Function to create running averages of daily data
def CreateRunningAverages(full_df_dict):
    for country in full_df_dict:
        # Use DataFrame.rolling() to generate running average data
        rolling = full_df_dict[country].rolling(rolling_window)

        # Calculate the rolling mean of the whole data frame
        mean = rolling.mean()

        # Add the rolling mean columns to the data frame
        full_df_dict[country]['MeanDailyConfirmed'] = mean['DailyConfirmed'].round()
        full_df_dict[country]['MeanDailyDeaths'] = mean['DailyDeaths'].round()
        full_df_dict[country]['MeanDailyRecovered'] = mean['DailyRecovered'].round()

# Create a Figure to add the data to
def CreateLineFigure(title        = 'Untitled Chart', 
                     x_axis_label = 'x', 
                     y_axis_label = 'y', 
                     x_axis_type  = 'linear', 
                     y_axis_type  = 'linear',
                     x_tooltip    = None,
                     y_tooltip    = None):
    # Create the figure
    plot = figure(title         = title, 
                  x_axis_label  = x_axis_label, 
                  y_axis_label  = y_axis_label, 
                  x_axis_type   = x_axis_type, 
                  y_axis_type   = y_axis_type,
                  plot_width    = plot_width,
                  plot_height   = plot_height,
                  tools         = tools,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Ensure the figure size is responsive to window size changes
    plot.sizing_mode = 'stretch_both'

    # Add a fake legend to enable setting position in common function before adding data
    legend1 = Legend()
    plot.add_layout(legend1)

    # Set the legend location to top left, out of the way
    plot.legend.location='top_left'

    # Allow the data series to be turned on and off by clicking the legend
    plot.legend.click_policy='hide'

    # Set legend transparency
    plot.legend.background_fill_alpha = 0.75

    # Check that both tooltips have actually been set
    if (x_tooltip != None) and (y_tooltip != None):
        formatters_str = None

        # If the tooltip is a date time then we need to add a formatter and format
        if (x_axis_type == 'datetime'):
            x_tooltip_str  = '@' + x_tooltip + '{%F}'
            formatters_str = '@' + x_tooltip

            # Format the axis
            plot.xaxis.formatter = DatetimeTickFormatter(days='%d/%m')
        else:
            x_tooltip_str  = '@' + x_tooltip

            # Format the axis
            plot.xaxis.formatter = NumeralTickFormatter(format='0.0a')

        # Same for y-axis
        if (y_axis_type == 'datetime'):
            y_tooltip_str  = '@' + y_tooltip + '{%F}'
            formatters_str = '@' + y_tooltip

            # Format the axis
            plot.yaxis.formatter = DatetimeTickFormatter(days='%d/%m')
        else:
            y_tooltip_str  = '@' + y_tooltip

            # Format the axis
            plot.yaxis.formatter = NumeralTickFormatter(format='0.0a')

        # Set the tooltips
        tooltips = [('Country', '$name'),
                    (x_axis_label, x_tooltip_str),
                    (y_axis_label, y_tooltip_str)]

        # Formatter for dates
        if formatters_str != None:
            formatters = { formatters_str : 'datetime' }
        else:
            formatters = None

        # Add the tooltip
        AddToolTip(plot, tooltips, formatters)

    return plot

# Function to add and format a tooltip
def AddToolTip(plot, tooltip, formatter = None):
    # Create a hover tool
    hover_tool = HoverTool()

    # Set the tooltips
    hover_tool.tooltips = tooltip
    
    # Formatter for dates
    hover_tool.formatters = formatter

    # Add the tooltip
    plot.add_tools(hover_tool)

    return hover_tool

# Return the tail of a frame once the values in the requested column have got above a certain level
def GetTail(country_df_dict, index_label, column, threshold = 0):
    # Get the first index where the data is above the threshold
    first_index = np.argmax(country_df_dict[column] > threshold)

    # Only tail if it's necessary, create a deep copy as we're adding a new column for the day count
    if first_index > 0:
        tailed_df = copy.deepcopy(country_df_dict.tail(-first_index))
    else:
        tailed_df = copy.deepcopy(country_df_dict)

    # Insert the Day Count column
    tailed_df[index_label] = np.arange(len(tailed_df))
    
    # Return the new Data Frame
    return tailed_df

# Return a modified copy of a data frame with dates shifted to bars don't stack
def BarShiftDates(country_df, country_number, number_of_countries):
    # Make a copy of the data for modification
    new_df = copy.deepcopy(country_df)

    # Shift the dates by the proportion of a day
    new_df['date'] += timedelta(days=((country_number + 0.5) / number_of_countries))

    # Return the new data frame
    return new_df

# Creates a plot of deaths against time
def CreateDeathsAgainstTimePlot(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'deaths'

    # Create the figure
    plot = CreateLineFigure('Deaths against Time (Line)', 
                            'Date',
                            'Deaths',
                            'datetime',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        column_data = ColumnDataSource(full_df_dict[country])
        new_line = plot.line(x=x_data,
                             y=y_data,
                             source=column_data,
                             name=country,
                             line_color=colour_dict[country])

        glyph_dict[country].append(new_line)

    return plot

# Creates a plot of deaths against time
def CreateActiveAgainstTimePlot(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'ActiveCases'

    # Create the figure
    plot = CreateLineFigure('Active Cases against Time', 
                            'Date',
                            'Active Cases',
                            'datetime',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        column_data = ColumnDataSource(full_df_dict[country])
        new_line = plot.line(x=x_data,
                             y=y_data,
                             source=column_data,
                             name=country,
                             line_color=colour_dict[country])

        glyph_dict[country].append(new_line)

    return plot

# Creates a plot of deaths against time
def CreateUKPlot(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'

    # Create the figure
    plot = CreateLineFigure('UK Stats', 
                            'Date',
                            'Number',
                            'datetime',
                            'log')

    column_data = ColumnDataSource(full_df_dict['United Kingdom'])
    deaths_line = plot.line(x=x_data,
                            y='deaths',
                            source=column_data,
                            legend_label='UK Deaths',
                            line_color='red')

    confirmed_line = plot.line(x=x_data,
                               y='confirmed',
                               source=column_data,
                               legend_label='UK Confirmed Cases',
                               line_color='blue')

    recovered_line = plot.line(x=x_data,
                               y='recovered',
                               source=column_data,
                               legend_label='UK Recoveries',
                               line_color='green')

    active_line = plot.line(x=x_data,
                            y='ActiveCases',
                            source=column_data,
                            legend_label='UK Active Cases',
                            line_color='mediumaquamarine')

    formatters = { '@date' : 'datetime' }

    # Setup the tooltips
    tooltips = [('Deaths', '@deaths'),
                ('Date', '@date{%F}')]

    # Add the tooltip
    hover_tool_deaths = AddToolTip(plot, tooltips, formatters)

    # Setup the tooltips
    tooltips = [('Confirmed', '@confirmed'),
                ('Date', '@date{%F}')]

    # Add the tooltip
    hover_tool_confirmed = AddToolTip(plot, tooltips, formatters)

    # Setup the tooltips
    tooltips = [('Recovered', '@recovered'),
                ('Date', '@date{%F}')]

    # Add the tooltip
    hover_tool_recovered = AddToolTip(plot, tooltips, formatters)

    # Setup the tooltips
    tooltips = [('Active Cases', '@ActiveCases'),
                ('Date', '@date{%F}')]

    # Add the tooltip
    hover_tool_active = AddToolTip(plot, tooltips, formatters)

    # Ensure the hover tool only renders on the circles
    hover_tool_deaths.renderers    = [deaths_line]
    hover_tool_confirmed.renderers = [confirmed_line]
    hover_tool_recovered.renderers = [recovered_line]
    hover_tool_active.renderers    = [active_line]

    return plot

# Creates a plot of confirmed cases per captita against GDP per capita
def CreateConfirmedAgainstGDP_PCPlot(country_data):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'GDP_PC'
    y_data = 'ConfPerCap'

    # Create the figure
    plot = CreateLineFigure('Confirmed against GDP PC', 
                            'GDP Per Capita',
                            'Confirmed Per Capita',
                            'log',
                            'log')

    # Extract the columns we want
    new_df = copy.deepcopy(country_data[['Country', y_data, x_data]])

    # Create Linear Regression object
    linear_regressor = LinearRegression()

    # Get the data we are interested in into numpy arrays
    x_reg_data = new_df[x_data].values.reshape(-1, 1)
    y_reg_data = new_df[y_data].values.reshape(-1, 1)

    # Perform linear regression
    linear_regressor.fit(x_reg_data, y_reg_data)

    # Generate a prediction line
    x_values = np.arange(x_reg_data.min(), x_reg_data.max(),1000).reshape(-1, 1)
    y_prediction = linear_regressor.predict(x_values)
    pred_df = pd.DataFrame()
    pred_df[x_data] = x_values[:,0]
    pred_df[y_data] = y_prediction[:,0]

    # Calculate the R-Squared vales
    r_squared = linear_regressor.score(x_reg_data, y_reg_data)

    # Create a circle scatter plot for the countries
    g1 = plot.circle(x=x_data,
                     y=y_data,
                     size=5,
                     source=ColumnDataSource(new_df))

    # Create a linear regression line with the R-Squared value as a legend
    plot.line(x=x_data,
              y=y_data,
              source=ColumnDataSource(pred_df),
              line_color='red',
              legend_label = 'R-Squared = ' + str(r_squared))

    # Setup the tooltips
    tooltips = [('Country', '@Country'),
                ('Confirmed Per Capita', '@' + y_data),
                ('GDP Per Capita', '@' + x_data)]

    # Add the tooltip
    hover_tool = AddToolTip(plot, tooltips)

    # Ensure the hover tool only renders on the circles
    hover_tool.renderers = [g1]

    # Format the axes
    plot.xaxis.formatter = NumeralTickFormatter(format='0.0a')
    plot.yaxis.formatter = NumeralTickFormatter(format='0.0a')

    return plot

# Creates a bar chart of daily deaths in each country
def PlotBarDailyDeaths(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'DailyDeaths'

    # Counter for the countries
    country_number = 0

    # Create the figure
    plot = CreateLineFigure('Deaths against Time (Bar)', 
                            'Date', 
                            'Deaths', 
                            'datetime', 
                            'linear',
                            x_data,
                            y_data)

    # Loop through the countries to add each data set
    for country in countries:
        # Exclude the world figures as it skews the chart
        if country != 'World':
            # Shift the dates by the proportion of a day, minus one as world not included
            shifted_df = BarShiftDates(full_df_dict[country], country_number, len(countries))

            # Add this new data frame to a column data source
            column_data = ColumnDataSource(shifted_df.tail(14))

            # Plot a bar chart of daily deaths
            plot.vbar(x=x_data,
                      bottom=0, 
                      top=y_data,
                      source=column_data, 
                      legend_label=country, 
                      color=colour_dict[country], 
                      name=country,
                      width=timedelta(days=1/(len(countries))))

            # Increment the country counter
            country_number += 1

    return plot

# Creates a bar chart of daily confirmed cases in each country
def PlotBarDailyConfirmed(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'DailyConfirmed'

    # Counter for the countries
    country_number = 0

    # Create the figure
    plot = CreateLineFigure('Confirmed Cases against Time (Bar)', 
                            'Date', 
                            'Confirmed Cases', 
                            'datetime', 
                            'linear',
                            x_data,
                            y_data)

    # Loop through the countries to add each data set
    for country in countries:
        # Exclude the world figures as it skews the chart
        if country != 'World':
            # Shift the dates by the proportion of a day, minus one as world not included
            shifted_df = BarShiftDates(full_df_dict[country], country_number, len(countries))

            # Add this new data frame to a column data source
            column_data = ColumnDataSource(shifted_df.tail(14))

            # Plot a bar chart of daily deaths
            plot.vbar(x=x_data,
                      bottom=0, 
                      top=y_data,
                      source=column_data, 
                      legend_label=country, 
                      color=colour_dict[country], 
                      name=country,
                      width=timedelta(days=1/(len(countries))))

            # Increment the country counter
            country_number += 1

    return plot

# Function to chart the daily deaths since the first one for each country
def CreateDailyDeathsSinceFirst(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'DayCount'
    y_data = 'MeanDailyDeaths'

    # Create the figure
    plot = CreateLineFigure('Daily Deaths Since First', 
                            'Day',
                            'Average Daily Deaths',
                            'linear',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        # Exclude world from this as it skews the chart
        if country != 'World':
            # Get a copy of the data once cases get above a certain level
            tailed_df = GetTail(full_df_dict[country], x_data, y_data, start_after)

            # Create a column data source for the new Data Frame
            column_data = ColumnDataSource(tailed_df)

            # Plot this data frame
            new_line = plot.line(x=x_data,
                                 y=y_data,
                                 source=column_data,
                                 name=country,
                                 line_color=colour_dict[country])

            glyph_dict[country].append(new_line)

    return plot

# Function to chart the daily deaths since the first one for each country
def CreateDailyConfirmedCases(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'MeanDailyConfirmed'

    # Create the figure
    plot = CreateLineFigure('Daily Confirmed Cases', 
                            'Date',
                            'Average Daily Confirmed Cases',
                            'datetime',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        # Exclude world from this as it skews the chart
        if country != 'World':
            # Get a copy of the data once cases get above a certain level
            df = full_df_dict[country]

            # Create a column data source for the new Data Frame
            column_data = ColumnDataSource(df)

            # Plot this data frame
            new_line = plot.line(x=x_data,
                                 y=y_data,
                                 source=column_data,
                                 name=country,
                                 line_color=colour_dict[country])

            glyph_dict[country].append(new_line)

    return plot

# Function to chart the daily deaths since the first one for each country
def CreateTotalDeathsSinceFirst(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'DayCount'
    y_data = 'deaths'

    # Create the figure
    plot = CreateLineFigure('Total Deaths Since First', 
                            'Day',
                            'Deaths',
                            'linear',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        # Exclude world from this as it skews the chart
        if country != 'World':
            # Get a copy of the data once cases get above a certain level
            tailed_df = GetTail(full_df_dict[country], x_data, y_data, start_after)

            # Create a column data source for the new Data Frame
            column_data = ColumnDataSource(tailed_df)

            # Plot this data frame
            new_line = plot.line(x=x_data,
                                 y=y_data,
                                 source=column_data,
                                 name=country,
                                 line_color=colour_dict[country])

            glyph_dict[country].append(new_line)

    return plot

# Creates a plot of rolling average new deaths against total deaths
def CreateNewConfirmedAgainstTotalConfirmedPlot(full_df_dict):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'confirmed'
    y_data = 'MeanDailyConfirmed'

    # Create the figure
    plot = CreateLineFigure('Mean Daily Confirmed against Total Confirmed', 
                            'Total Confirmed',
                            'Mean Daily Confirmed',
                            'log',
                            'log')

    # Loop through the countries to add each line
    for country in glyph_dict:
        column_data = ColumnDataSource(full_df_dict[country])
        # Plot this data frame
        new_line = plot.line(x=x_data,
                             y=y_data,
                             source=column_data,
                             name=country,
                             line_color=colour_dict[country])

        glyph_dict[country].append(new_line)

    # Setup the tooltips
    tooltips = [('Country', '$name'),
                ('Confirmed Cases', '@confirmed'),
                ('Mean Daily Confirmed Cases', '@MeanDailyConfirmed'),
                ('Date', '@date{%F}')]

    # Add the date formatter
    formatters = { '@date' : 'datetime' }

    # Add the tooltip
    AddToolTip(plot, tooltips, formatters)

    return plot

# Create a map plot of latest confirmed cases
def ConfirmedMapPlot(full_df_dict, country_data):
    # Get the tile provider
    tile_provider = get_provider(WIKIMEDIA)

    # Create the plot
    plot = figure(x_axis_type="mercator", 
                  y_axis_type="mercator",
                  title='Confirmed Map Plot', 
                  tools = tools,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Add the tile streamer to the plot
    plot.add_tile(tile_provider)

    # Set the sizing mode
    plot.sizing_mode = 'stretch_both'

    # Transform from WGS83 to Web Mercator projection
    merc_geo_json_df = country_data.to_crs('EPSG:3857')

    # Add the transformed data to a GeoJSONDataSource
    geosource = GeoJSONDataSource(geojson = merc_geo_json_df.to_json())

    # Set the palette and min/max range for the colour mapper
    palette = brewer['Reds'][8]
    palette = palette[::-1]
    min_range = country_data['Confirmed'].min()
    max_range = country_data['Confirmed'].max()

    # Create the colour mapper
    color_mapper = LogColorMapper(palette = palette, low = min_range, high = max_range)

    # Create a tick formatter
    format_tick = NumeralTickFormatter(format='0.0a')

    # Create a Log Ticker
    log_ticker = LogTicker()

    # Create the colour bar which will go on the right
    color_bar = ColorBar(color_mapper=color_mapper,
                         label_standoff=18,
                         formatter=format_tick,
                         border_line_color=None,
                         location = (0, 0),
                         ticker=log_ticker)

    # Add the data to the circle plot
    plot.patches(xs='xs', 
                 ys='ys', 
                 source=geosource, 
                 line_color='red',
                 fill_alpha=0.8, 
                 fill_color={'field' : 'Confirmed', 'transform' : color_mapper})

    # Add the colour bar
    plot.add_layout(color_bar, 'right')

    # Setup the tooltips
    tooltips = [('Country', '@Country'),
                ('Confirmed', '@Confirmed')]

    # Add the tooltip
    AddToolTip(plot, tooltips)

    return plot

# Create a map plot of latest confirmed cases per capita
def ConfPerCapMapPlot(full_df_dict, country_data):
    # Get the tile provider
    tile_provider = get_provider(WIKIMEDIA)

    # Create the plot
    plot = figure(x_axis_type="mercator", 
                  y_axis_type="mercator",
                  title='Confirmed PC Map Plot', 
                  tools = tools,
                  active_drag   = 'pan',
                  active_scroll = 'wheel_zoom')

    # Add the tile streamer to the plot
    plot.add_tile(tile_provider)

    # Set the sizing mode
    plot.sizing_mode = 'stretch_both'

    # Transform from WGS83 to Web Mercator projection
    merc_geo_json_df = country_data.to_crs('EPSG:3857')

    # Add the transformed data to a GeoJSONDataSource
    geosource = GeoJSONDataSource(geojson = merc_geo_json_df.to_json())

    # Set the palette and min/max range for the colour mapper
    palette = brewer['Blues'][8]
    palette = palette[::-1]
    min_range = country_data['ConfPerCap'].min()
    max_range = country_data['ConfPerCap'].max()

    # Create the colour mapper
    color_mapper = LogColorMapper(palette = palette, low = min_range, high = max_range)

    # Create a tick formatter
    format_tick = NumeralTickFormatter(format='0.0a')

    # Create a Log Ticker
    log_ticker = LogTicker()

    # Create the colour bar which will go on the right
    color_bar = ColorBar(color_mapper=color_mapper,
                         label_standoff=18,
                         formatter=format_tick,
                         border_line_color=None,
                         location = (0, 0),
                         ticker=log_ticker)

    # Add the data to the circle plot
    plot.patches(xs='xs', 
                 ys='ys', 
                 source=geosource, 
                 line_color='blue',
                 fill_alpha=0.8, 
                 fill_color={'field' : 'ConfPerCap', 'transform' : color_mapper})

    # Add the colour bar
    plot.add_layout(color_bar, 'right')

    # Setup the tooltips
    tooltips = [('Country', '@Country'),
                ('Confirmed per 100m Population', '@ConfPerCap')]

    # Add the tooltip
    AddToolTip(plot, tooltips)

    return plot

# Creates a plot of deaths against time
def CreateConfirmedPerCapAgainstTimePlot(full_df_dict, country_data):
    # Set the x and y data here so it only needs to be changed in one place
    x_data = 'date'
    y_data = 'DeathPerCap'

    # Create the figure
    plot = CreateLineFigure('Deaths per Capita against Time', 
                            'Date',
                            'Deaths per Capita',
                            'datetime',
                            'log',
                            x_data,
                            y_data)

    # Loop through the countries to add each line
    for country in glyph_dict:
        # Skip world as it isn't in the list of locations
        if country != 'World':
            country_dict = full_df_dict[country]

            # Extract the row from the country specific info for this country
#            c_df = country_data[country_data['Country'] == country]

            pop = country_data[country_data['Country'] == country]['Population']

            # Divide the total deaths by the population, there must be a 'proper' way to get the value
            country_dict[y_data] = country_dict['deaths'] / pop.to_numpy()

            # Add to a column data source
            column_data = ColumnDataSource(country_dict)

            # Plot this data frame
            new_line = plot.line(x=x_data,
                                 y=y_data,
                                 source=column_data,
                                 name=country,
                                 line_color=colour_dict[country])

            glyph_dict[country].append(new_line)

    return plot

# Main function
def main():
    # For timing
    t1=datetime.now()

    # Get the data
    json_data = getData()

    # For timing
    t2=datetime.now()

    # Set the save file
    output_file(temp_filename, 'Covid-19 Charts')

    # Arrays for the plots and tabs
    plot_array = []
    tab_array  = []

    # Get the column data for this country
    full_df_dict = ParseData(json_data)

    # Read in and update the country specific data
    country_data = ParseCountryData(full_df_dict)

    # Create the charts, appending them to an array of tabs
    plot_array.append(CreateNewConfirmedAgainstTotalConfirmedPlot(full_df_dict))
    plot_array.append(CreateUKPlot(full_df_dict))
    plot_array.append(CreateDailyConfirmedCases(full_df_dict))
    plot_array.append(CreateActiveAgainstTimePlot(full_df_dict))
    plot_array.append(CreateDeathsAgainstTimePlot(full_df_dict))
    plot_array.append(CreateConfirmedAgainstGDP_PCPlot(country_data))
    plot_array.append(CreateConfirmedPerCapAgainstTimePlot(full_df_dict, country_data))
    plot_array.append(ConfirmedMapPlot(full_df_dict, country_data))
    plot_array.append(ConfPerCapMapPlot(full_df_dict, country_data))
    plot_array.append(CreateTotalDeathsSinceFirst(full_df_dict))
    plot_array.append(CreateDailyDeathsSinceFirst(full_df_dict))
    plot_array.append(PlotBarDailyDeaths(full_df_dict))
    plot_array.append(PlotBarDailyConfirmed(full_df_dict))

    # For timing
    t3=datetime.now()

    # Put the plots into tabs
    for plot in plot_array:
        tab_array.append(Panel(child=plot, title=plot.title.text))

    # Create the tabs
    tabs=Tabs(tabs=tab_array, sizing_mode='stretch_both')

    # Create a list of all the countries for the checkbox group, ensuring that the countries
    # defined at the top of this file are at the start of the list
    country_list = list()

    # List to contain the checkboxes selected by default
    selected_list = list()

    # Counter to fill the selected list
    counter = 0

    # Loop through the countries in the alphabetically sorted glyph dict as this is what will actually be plotted
    for country in sorted(glyph_dict):
        # If the country is one defined at the top, insert this as the start
        if country in countries:
            country_list.insert(counter, country)

            # Ensure the checkbox is ticked
            selected_list.append(counter)

            # Increment the counter
            counter += 1

            # Make the glyphs associated with this country visible
            for glyph in glyph_dict[country]:
                glyph.visible = True

        else:
            # Add the country to the end of the list
            country_list.append(country)

            # Make the glyphs associated with this country hidden by default
            for glyph in glyph_dict[country]:
                glyph.visible = False

    # Move World entry to be after the default entries
    country_list.insert(counter, country_list.pop(country_list.index('World')))

    # Set up the Checkbox Group
    cb_group = CheckboxGroup(labels=country_list, 
                             width=160,
                             height=595, 
                             sizing_mode='fixed', 
                             css_classes=['scrollable'],
                             active=selected_list)

    # Set up the layout 
    div2 = Div(text="""<a href=https://gitlab.com/SSchleising/python-charts/-/tree/master#credits target="_blank">Credits</a>""",
                width=cb_group.width, height=5, sizing_mode='fixed', style={'overflow-y':'scroll','height':'30px'})
    column1 = column(children=[cb_group, div2], width=cb_group.width+5, height=(cb_group.height + div2.height), sizing_mode='fixed')
    column2 = column(children=[tabs], sizing_mode='stretch_both')
    row1 = row(children=[column1, column2], sizing_mode='stretch_both')

    # Define the callback for the checkbox group in JavaScript
    callback = CustomJS(args=dict(checkbox=cb_group, glyph_dict=glyph_dict), code="""
        var i, j;

        // Loop through all the checkboxes
        for (i = 0; i < checkbox.labels.length; i++) {
            // If the list of active checkboxes includes this index
            if (checkbox.active.includes(i)) {
                // Loop through the glyphs associated with this checkbox and make them visible
                for (j in glyph_dict[checkbox.labels[i]]) {
                    glyph_dict[checkbox.labels[i]][j].visible = true;
                }
            }
            // If the list of active checkboxes includes this index
            else {
                // Loop through the glyphs associated with this checkbox and make them hidden
                for (j in glyph_dict[checkbox.labels[i]]) {
                    glyph_dict[checkbox.labels[i]][j].visible = false;
                }
            }
        }
    """)

    # Add the callback to the checkbox group
    cb_group.js_on_change('active', callback)

    # Show the chart as a set of tabs
    save(row1)

    # Update the html file to add the definition of the scrollable class used by the checkbox group
    with open(temp_filename) as soup_file:
        soup = BeautifulSoup(soup_file, 'html.parser')

        # Add scrollable class definition to the header
        new_tag = soup.new_tag('style')
        new_tag.string = 'div.scrollable {overflow: auto;}'
        soup.head.append(new_tag)

        # Add the favicon
        favicon_tag = soup.new_tag('link', rel='icon', type='image/ico', href='favicon.ico')
        soup.head.append(favicon_tag)

        # Add Google Analytics
        tag_manager = soup.new_tag('script', src="https://www.googletagmanager.com/gtag/js?id=UA-174908278-1")
        tag_manager.attrs['async'] = None

        analytics_tag = soup.new_tag('script')
        analytics_tag.string = "\n  window.dataLayer = window.dataLayer || [];\n  function gtag(){dataLayer.push(arguments);}\n  gtag('js', new Date());\n  gtag('config', 'UA-174908278-1');\n"

        soup.head.append(tag_manager)
        soup.head.append(analytics_tag)

        # Write the update to a new file
        with open(output_filename, 'w') as outfile:
            outfile.write(str(soup))

    # Show the chart
    webbrowser.open('file://' + os.path.realpath(output_filename))
    
    # For timing
    t4=datetime.now()

    # Display the timings
    print('Time to download data :', t2-t1)
    print('Time to process data  :', t3-t2)
    print('Time to show data     :', t4-t3)

# Call main
main()
