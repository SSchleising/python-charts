# Python Charts

Create various graphs to chart the course of the Coronavirus

## Usage

Use the following template to produce a new graph

    # Creates a plot
    def CreatePlot(full_df_dict):
        # Set the x and y data here so it only needs to be changed in one place
        x_data = 'date'
        y_data = 'deaths'

        # Create the figure
        plot = CreateLineFigure('Deaths against Time (Line)', 
                                'Date',
                                'Deaths',
                                'datetime',
                                'log',
                                x_data,
                                y_data)

        # Loop through the countries to add each line
        for country in countries:
            # Do any work you may need to do on the Data Frame here
            new_df = perform_some_dead_hard_sums()

            # Assign the Pandas data frame you want to use to a Bokeh Column Data Source
            column_data = ColumnDataSource(new_df)

            # Call plot.plot_type
            plot.line(x=x_data,
                      y=y_data,
                      source=column_data,
                      legend_label=country, 
                      name=country,
                      line_color=countries[country])

        # Return the plot
        return plot

Then in <code>main()</code> call the above function and append the result to the plot array, after this it will automatically get added to the set of tabs and appear in the chart panel

## Adding New Countries

Add the country name (which must be the same as that in the source json) to the countries dict, giving it a colour or using the <code>random_colour()</code> function

## Pre-calculated Data

The following data is available already calculated in the basic data frames

- From json
    - Date
        - String: 'date'
        - The dates for each data entry (row)
    - Confirmed Cases
        - String: 'confirmed'
        - The cumulative total of confirmed cases
    - Deaths
        - String: 'deaths'
        - The cumulative total deaths
    - Recovered
        - String: 'recovered'
        - The cumulative number of people who have recovered
- Calculated
    - Daily Confirmed Cases
        - String: 'DailyConfirmed'
        - The newly confirmed cases on that day
    - Daily Deaths
        - String: 'DailyDeaths'
        - The new deaths on that day
    - DailyRecovered
        - String: 'DailyRecovered'
        - The new number of people who have recovered on that day
    - Rolling 7 Day Average of Confirmed Cases
        - String: 'MeanDailyConfirmed'
        - The 7 day rolling average of newly confirmed cases
    - Rolling 7 Day Average New Deaths
        - String: 'MeanDailyDeaths'
        - The 7 day rolling average of new deaths
    - Rolling 7 Day Average of Recovered People
        - String: 'MeanDailyRecovered'
        - The 7 day rolling average of newly recovered people

## Credits

The following libraries and data sources have been used in creating this project

- [CSSE at Johns Hopkins University](https://github.com/CSSEGISandData) for the base data
- [Rodrigo Pomber](https://github.com/pomber)
  - For the [json version](https://pomber.github.io/covid19/timeseries.json) of the Johns Hopkins data
- [Bokeh](https://docs.bokeh.org/en/latest/index.html)
- [Numpy](https://numpy.org/)
- [Pandas](https://pandas.pydata.org/)
- [GeoPy](https://geopy.readthedocs.io/en/stable/) for the latitude and longitude data for each country
- [CartoDB Positron Maps](https://carto.com/location-data-services/basemaps/)
  - Which sources the data from [Open Street Map](http://openstreetmap.org/copyright)
- [Beautiful Soup](https://www.crummy.com/software/BeautifulSoup/) used to modify the html file after Bokeh creates it
- [Pandas Data Reader](https://pandas-datareader.readthedocs.io/en/latest/index.html) for access to World Bank population data
- [World Bank](https://data.worldbank.org/) for the population data itself
- [pycountry](https://pypi.org/project/pycountry/) for the ISO codes to access the World Bank data
- [Natural Earth](http://www.naturalearthdata.com/) for the country outline data

## Available Countries

- Afghanistan
- Albania
- Algeria
- Andorra
- Angola
- Antigua and Barbuda
- Argentina
- Armenia
- Australia
- Austria
- Azerbaijan
- Bahamas
- Bahrain
- Bangladesh
- Barbados
- Belarus
- Belgium
- Benin
- Bhutan
- Bolivia
- Bosnia and Herzegovina
- Brazil
- Brunei
- Bulgaria
- Burkina Faso
- Cabo Verde
- Cambodia
- Cameroon
- Canada
- Central African Republic
- Chad
- Chile
- China
- Colombia
- Congo (Brazzaville)
- Congo (Kinshasa)
- Costa Rica
- Cote d'Ivoire
- Croatia
- Diamond Princess
- Cuba
- Cyprus
- Czechia
- Denmark
- Djibouti
- Dominican Republic
- Ecuador
- Egypt
- El Salvador
- Equatorial Guinea
- Eritrea
- Estonia
- Eswatini
- Ethiopia
- Fiji
- Finland
- France
- Gabon
- Gambia
- Georgia
- Germany
- Ghana
- Greece
- Guatemala
- Guinea
- Guyana
- Haiti
- Holy See
- Honduras
- Hungary
- Iceland
- India
- Indonesia
- Iran
- Iraq
- Ireland
- Israel
- Italy
- Jamaica
- Japan
- Jordan
- Kazakhstan
- Kenya
- Korea, South
- Kuwait
- Kyrgyzstan
- Latvia
- Lebanon
- Liberia
- Liechtenstein
- Lithuania
- Luxembourg
- Madagascar
- Malaysia
- Maldives
- Malta
- Mauritania
- Mauritius
- Mexico
- Moldova
- Monaco
- Mongolia
- Montenegro
- Morocco
- Namibia
- Nepal
- Netherlands
- New Zealand
- Nicaragua
- Niger
- Nigeria
- North Macedonia
- Norway
- Oman
- Pakistan
- Panama
- Papua New Guinea
- Paraguay
- Peru
- Philippines
- Poland
- Portugal
- Qatar
- Romania
- Russia
- Rwanda
- Saint Lucia
- Saint Vincent and the Grenadines
- San Marino
- Saudi Arabia
- Senegal
- Serbia
- Seychelles
- Singapore
- Slovakia
- Slovenia
- Somalia
- South Africa
- Spain
- Sri Lanka
- Sudan
- Suriname
- Sweden
- Switzerland
- Taiwan*
- Tanzania
- Thailand
- Togo
- Trinidad and Tobago
- Tunisia
- Turkey
- Uganda
- Ukraine
- United Arab Emirates
- United Kingdom
- Uruguay
- US
- Uzbekistan
- Venezuela
- Vietnam
- Zambia
- Zimbabwe
- Dominica
- Grenada
- Mozambique
- Syria
- Timor-Leste
- Belize
- Laos
- Libya
- West Bank and Gaza
- Guinea-Bissau
- Mali
- Saint Kitts and Nevis
- Kosovo
- Burma
- MS Zaandam
- Botswana
- Burundi
- Sierra Leone
- Malawi
- South Sudan
- Western Sahara
- Sao Tome and Principe
- Yemen
- World