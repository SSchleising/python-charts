import numpy as numpy
import pandas as pd
import geopandas
import plotly.graph_objects as go

def main():
    df = geopandas.read_file('/Users/steve/SynologyDrive/Coding/Python/BokehCharts/python-charts/data/ne_110m_admin_0_countries/ne_110m_admin_0_countries.shp')

    for row in df.index:
        print(df['NAME'][row], df['POP_EST'][row])

main()