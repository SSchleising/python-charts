import json
import copy
import requests
import pycountry
from datetime import datetime, date, timedelta
import numpy as np
import pandas as pd
from geopy.geocoders import Nominatim
import time
import csv
from pandas_datareader import wb
#from bokeh.tile_providers import CARTODBPOSITRON, get_provider


# The URL containing the data
json_url = 'https://pomber.github.io/covid19/timeseries.json'

# # The country we're looking at
# countries = {'Italy'          : 'blue',
#              'Spain'          : 'orange',
#              'France'         : 'purple',
#              'United Kingdom' : 'green',
#              'US'             : 'red',
#              'Australia'      : 'brown',
#              'India'          : 'magenta',
#              'Sweden'         : 'black',
#              'Germany'        : 'cyan'}

# The country we're looking at
countries = {'Italy'          : 'blue',
             'United Kingdom' : 'green',
             'Germany'        : 'cyan'}

# Some common chart defaults
plot_width  = 1200
plot_height = 650
tools       = 'pan, wheel_zoom, reset'

sheets  = 5
rows    = 4
columns = 3

# Function to get the data from the internet
def getData():
    # Get the data from the internet
    raw_data = requests.get(json_url)

    # Turn the data into a json object
    json_data = raw_data.json()

    # Return the data
    return json_data

def main():
    t1 = datetime.now()

    json_data = getData()

    # Create a dictionary, indexable by country name, to contain the data frames
    full_df_dict = {}

    t2 = datetime.now()

    # Loop through each country in the json data, create a data frame and add it to the dictionary
    for country in json_data:
        full_df_dict[country] = pd.DataFrame(json_data[country])

    # Whole world stats are not available, so create an empty data frame to contain them
    world_df = pd.DataFrame(data = np.zeros(full_df_dict['US'].shape), columns=full_df_dict['US'].columns)

    # Set the dates from the US data frame and US is the least typing...
    world_df['date'] = full_df_dict['US']['date']

    t3 = datetime.now()

    # Loop through the countries once more to sum the columns
    for country in full_df_dict:
        world_df['confirmed'] += full_df_dict[country]['confirmed']
        world_df['deaths']    += full_df_dict[country]['deaths']
        world_df['recovered'] += full_df_dict[country]['recovered']

    # Add the world entry to the dictionary
    full_df_dict['World'] = world_df

    df_data = pd.read_csv('data/locations.csv')
    print(df_data.loc[50, 'Population'])
    c_df = df_data[df_data['Country'] == 'US']

    print(c_df)
    print(c_df.index)
    print(c_df.loc[c_df.index[0], 'Population'])

#    pd.to_csv
#    print(pop_data)

    # matches = wb.search('population.*total')

    # print(matches)

    # country_alpha_3_list = list()

    # for country in df_data['Country']:
    #     try:
    #         country_list = pycountry.countries.search_fuzzy(country)
    #         country_alpha_3_list.append(country_list[0].alpha_3)
    #     except:
    #         country_alpha_3_list.append('TBD')
    #         print("Didnt like", country, country_list[0].alpha_3)

    # df_data['ISO Codes'] = country_alpha_3_list

    # df_data.to_csv('data/locations2.csv')

    # print(country_alpha_3_list)

    # dat = wb.download(indicator='SP.POP.TOTL', country=country_alpha_3_list, start=2018, end=2018)
    # print(dat['SP.POP.TOTL']['Ecuador', '2018'])
    # print()
    # dat2 = dat['SP.POP.TOTL']
    # print(type(dat2))
    # print()
    # print(dat.index)
    # print()
    # for i in dat.index:
    #     print(i[0])
    #     print(dat['SP.POP.TOTL'][i])
#    print(dat.columns.levels)
#        print(dat.'SP.POP.TOTL')



#    print(pop_data['Country Name'], pop_data['Country Code'])

    # for i in pop_data['Country Name']:
    #     print(i)

    # confirmed_list = list()

    # for index, row in df_data.iterrows():
    #     country_df = full_df_dict[row['Country']]
    #     confirmed_list.append(country_df['confirmed'][len(country_df)-1])

    # df_data.insert(len(df_data.columns), 'Confirmed', confirmed_list)



    #     # Add the dailies
    #     for country in full_df_dict:
    #         # Use diff() to calculate the difference between this and last through the column
    #         daily_confirmed = full_df_dict[country]['confirmed'].diff()
    #         daily_deaths    = full_df_dict[country]['deaths'].diff()
    #         daily_recovered = full_df_dict[country]['recovered'].diff()

    #         # diff() sets the first etry to NaN, so set it to the first element value
    #         daily_confirmed[0] = full_df_dict[country]['confirmed'][0]
    #         daily_deaths[0]    = full_df_dict[country]['deaths'][0]
    #         daily_recovered[0] = full_df_dict[country]['recovered'][0]

    #         # Insert the new columns
    #         full_df_dict[country].insert(len(full_df_dict[country].columns), 'DailyConfirmed', daily_confirmed)
    #         full_df_dict[country].insert(len(full_df_dict[country].columns), 'DailyDeaths', daily_deaths)
    #         full_df_dict[country].insert(len(full_df_dict[country].columns), 'DailyRecovered', daily_recovered)

    # #        print(country)
    #         location = geolocator.geocode(country)
    #         print(country + ',' + str(location.latitude) + ',' + str(location.longitude))

    #         writer.writerow([country, str(location.latitude), str(location.longitude)])

    #         time.sleep(1)

    t4 = datetime.now()

    # test = full_df_dict['United Kingdom']['DailyDeaths']

    # first_index = np.argmax(test>3)

    # last_item_count = len(test) - first_index

    # tail = copy.deepcopy(full_df_dict['United Kingdom'].tail(last_item_count))

    # tail.insert(len(tail.columns), 'DayCount', np.arange(len(tail)))

    # #print(tail)

    # rolling = tail.rolling(7)

#    print(rolling.mean()['DailyDeaths'])

#    tail.insert(len(tail.columns), 'RollingAverageDeaths', rolling.mean())

    # Display the timings
    print('Time to download data  :', t2-t1)
    print('Time to fill countries :', t3-t2)
    print('Time to calc world     :', t4-t3)


main()