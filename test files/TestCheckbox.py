# Import the required modules
import os
import webbrowser
import copy
import random
import requests
import json
import csv
import numpy as np
import pandas as pd
import geopandas
from datetime import datetime, date, timedelta
from bokeh.plotting import figure, output_file, save, show
from bokeh.models import Panel, Tabs, ColumnDataSource, HoverTool, Legend, Div, CheckboxGroup, LogColorMapper, ColorBar, LogTickFormatter, GeoJSONDataSource, NumeralTickFormatter, CustomJS
from bokeh.layouts import row, column, layout
from bokeh.palettes import brewer
from bs4 import BeautifulSoup
from bokeh.tile_providers import WIKIMEDIA, CARTODBPOSITRON_RETINA, get_provider
from sklearn.linear_model import LinearRegression

plot1 = figure()
plot2 = figure()

x_values = np.arange(-10, 11)
p1y1_values = x_values
p1y2_values = x_values * x_values - 50
p1y3_values = -x_values
p2y1_values = np.sin(x_values)
p2y2_values = np.cos(x_values)
p2y3_values = np.tan(x_values)

data_frame = pd.DataFrame({'Xs' : x_values,
                           'Y1s' : p1y1_values,
                           'Y2s' : p1y2_values,
                           'Y3s' : p1y3_values,
                           'Y4s' : p2y1_values,
                           'Y5s' : p2y2_values,
                           'Y6s' : p2y3_values})

column_data = ColumnDataSource(data_frame)

g1 = plot1.line(x='Xs', y='Y1s', source=column_data, line_color='red')
g2 = plot1.line(x='Xs', y='Y2s', source=column_data, line_color='blue')
g3 = plot1.line(x='Xs', y='Y3s', source=column_data, line_color='green')
g4 = plot2.line(x='Xs', y='Y4s', source=column_data, line_color='red')
g5 = plot2.line(x='Xs', y='Y5s', source=column_data, line_color='blue')
g6 = plot2.line(x='Xs', y='Y6s', source=column_data, line_color='green')

#glyph_array = [['Red', 'Blue', 'Green'], [g1, g2, g3], [g4, g5, g6]]
glyph_array = [[g1, g2, g3], [g4, g5, g6]]

glyph_dict = {'Red'   : [g1, g4],
              'Blue'  : [g2, g5],
              'Green' : [g3, g6]}

cb = CheckboxGroup(labels=['Red', 'Blue', 'Green'], active=[0,1,2])

tab1 = Panel(child=plot1, title='Plot 1')
tab2 = Panel(child=plot2, title='Plot 2')
tabs = Tabs(tabs=[tab1, tab2])

row1 = row([cb, tabs])

array_array = []

array1 = [11, 12, 13]
array2 = [21, 22, 23]
array3 = [31, 32, 33]

array_array.append(array1)
array_array.append(array2)
array_array.append(array3)

print(array_array)

print()

for i in array_array:
    print(i)

callback = CustomJS(args=dict(line_array=glyph_array, checkbox=cb, glyph_dict=glyph_dict), code="""
    // console.log("Test1");
    // console.log(checkbox.active);
    var i, j;
//    for(i in line_array) {
//        for (j in line_array[i]) {
//            line_array[i][j].visible = false;
//            }
//        for (j in checkbox.active) {
            // console.log("Test2");
            // console.log(checkbox.labels[checkbox.active[j]]);
//            line_array[i][checkbox.active[j]].visible = true;
//        }
//    }
//    console.log(glyph_dict)
//    console.log(checkbox)

    for (i = 0; i < checkbox.labels.length; i++) {
//        console.log(checkbox.labels[i])
//        console.log(glyph_dict[checkbox.labels[i]])
        if (checkbox.active.includes(i)) {
            console.log(i, "is checked");
            for (j in glyph_dict[checkbox.labels[i]]) {
                glyph_dict[checkbox.labels[i]][j].visible = true;
            }
        }
        else {
            console.log(i, "is NOT checked");
            for (j in glyph_dict[checkbox.labels[i]]) {
                glyph_dict[checkbox.labels[i]][j].visible = false;
            }
        }
    }
""")

cb.js_on_change('active', callback)

print(cb.labels)

show(row1)
