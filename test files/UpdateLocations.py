import csv
from pandas_datareader import wb

# Modify these two values to generate a new column named <column_name> with data from <indicator>
indicator = 'NY.GDP.PCAP.KD'
column_name = 'GDP_PC'

with open('data/locations.csv') as csv_file:
    reader = csv.DictReader(csv_file)
    fieldnames = reader.fieldnames
    print(fieldnames)

    with open('data/locations2.csv', 'w') as csv_file:
        fieldnames.append(column_name)
        writer = csv.DictWriter(csv_file, fieldnames)

        writer.writeheader()

        for row in reader:
            try:
                dat = wb.download(indicator=indicator, country=row['ISO Codes'], start=2018, end=2018)
                print(row['Country'])
                for i in dat.index:
                    row[column_name] = dat[indicator][i]
                    print(dat[indicator][i])
                    writer.writerow(row)
            except:
                print("Didnt like", row['Country'])
                row[column_name] = 'TBD'
                writer.writerow(row)
                # break
