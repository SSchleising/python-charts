from bokeh.plotting import figure, output_file, show
from bokeh.models import GeoJSONDataSource, LinearColorMapper, ColorBar, NumeralTickFormatter, HoverTool
from bokeh.tile_providers import CARTODBPOSITRON, ESRI_IMAGERY, WIKIMEDIA, get_provider
import numpy as np
import pandas as pd
import json
import geopandas
from bokeh.sampledata.sample_geojson import geojson
from bokeh.palettes import brewer

output_file("tile.html")

# Read in the country data
country_data = pd.read_csv('data/locations.csv')

def geographic_to_web_mercator(lat, long):
    if abs(long) <= 180 and abs(lat) < 90:
        num = long * 0.017453292519943295
        x = 6378137.0 * num
        a = lat * 0.017453292519943295
        x_mercator = x
        y_mercator = 3189068.5 * np.log((1.0 + np.sin(a)) / (1.0 - np.sin(a)))
        return x_mercator, y_mercator
    else:
        print('Invalid coordinate values for conversion')        

# Function to add and format a tooltip
def AddToolTip(plot, tooltip, formatter = None):
    # Create a hover tool
    hover_tool = HoverTool()

    # Set the tooltips
    hover_tool.tooltips = tooltip
    
    # Formatter for dates
    hover_tool.formatters = formatter

    # Add the tooltip
    plot.add_tools(hover_tool)

def main():
    tile_provider = get_provider(WIKIMEDIA)

    # range bounds supplied in web mercator coordinates
    # p = figure(x_range=(-20000000, 20000000),
    #            y_range=(0, 20000000))
    p = figure()
    p.add_tile(tile_provider)

    x_merc, y_merc = geographic_to_web_mercator(lat=50.842941, long=-0.131312)

    # data = json.loads(geojson)
    # for i in range(len(data['features'])):
    #     data['features'][i]['properties']['Color'] = ['blue', 'red'][i%2]

    # geo_source = GeoJSONDataSource(geojson=json.dumps(data))

    geo_json_df = geopandas.read_file('data/ne_110m_admin_0_countries/ne_110m_admin_0_countries.shp')
    # geo_json_df = geopandas.read_file(geopandas.datasets.get_path('naturalearth_lowres'))

    print(geo_json_df.columns)

    geo_json_df.crs = 'epsg:4326'

    geo_json_df.set_geometry('geometry')

    print(geo_json_df[(geo_json_df['ADM0_A3'] == '-99')]['NAME_EN'])
    print(geo_json_df[(geo_json_df['NAME_EN'] == 'Kosovo')]['ADM0_A3'])


    print(type(country_data))

    # geo_json = json.loads(geo_json_df.to_json())

    merged_df = geopandas.GeoDataFrame(country_data.merge(geo_json_df, right_on='ADM0_A3', left_on='ISO Codes'))

    merged_df.crs = 'epsg:4326'

    merged_df.set_geometry('geometry')

    print(type(merged_df))

    merc_geo_json_df = merged_df.to_crs('EPSG:3857')
 
    geosource = GeoJSONDataSource(geojson = merc_geo_json_df.to_json())

    # Set the palette and min/max range for the colour mapper
    palette = brewer['Reds'][8]
    palette = palette[::-1]
    min_range = country_data['Population'].min()
    max_range = country_data['Population'].max()

    # Create the colour mapper
    color_mapper = LinearColorMapper(palette = palette, low = min_range, high = max_range)

    # Create a tick formatter
    format_tick = NumeralTickFormatter()

    # Create the colour bar which will go on the right
    color_bar = ColorBar(color_mapper=color_mapper,
                         label_standoff=18,
                         formatter=format_tick,
                         border_line_color=None,
                         location = (0, 0))

    # Add the colour bar
    p.add_layout(color_bar, 'right')

    # p.circle(x=x_merc, y=y_merc, size=20)
    p.patches('xs',
              'ys', 
              source = geosource, 
              fill_color = {'field' : 'Population', 'transform' : color_mapper}, 
              line_color = 'red', 
              line_width = 0.25, 
              fill_alpha = 0.8)

    p.aspect_ratio = 1

    p.sizing_mode='scale_height'

    # Setup the tooltips
    tooltips = [('Country', '@NAME_EN'),
                ('Confirmed', '@Population')]

    # Add the tooltip
    AddToolTip(p, tooltips)

    show(p)

main()