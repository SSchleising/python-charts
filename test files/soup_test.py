from bs4 import BeautifulSoup

output_filename = 'temp/index.html'

# Update the html file to add the definition of the scrollable class used by the checkbox group
with open(output_filename) as soup_file:
    soup = BeautifulSoup(soup_file, 'html.parser')

    # Add scrollable class definition to the header
    # print(soup.head)
    new_tag = soup.new_tag('style')
    new_tag.string = 'div.scrollable {overflow: auto;}'
    soup.head.append(new_tag)

    tag_manager = soup.new_tag('script', src="https://www.googletagmanager.com/gtag/js?id=UA-174908278-1")
    tag_manager.attrs['async'] = None

    analytics_tag = soup.new_tag('script')
    analytics_tag.string = "\n  window.dataLayer = window.dataLayer || [];\n  function gtag(){dataLayer.push(arguments);}\n  gtag('js', new Date());\n  gtag('config', 'UA-174908278-1');\n"

# <script>
#   window.dataLayer = window.dataLayer || [];
#   function gtag(){dataLayer.push(arguments);}
#   gtag('js', new Date());

#   gtag('config', 'UA-174908278-1');
# </script>


    print(tag_manager)
    print()
    print(analytics_tag)

    soup.head.append(tag_manager)
    soup.head.append(analytics_tag)
    
    print()
    print(soup.head)

    # with open('temp/index2.html', 'w') as outfile:
    #     outfile.write(str(soup))